package api

import (
	_ "gitlab.com/learn-microservice/api-gateway/api/docs"
	"github.com/gin-gonic/gin"
	swaggerfile "github.com/swaggo/files"
	v1 "gitlab.com/learn-microservice/api-gateway/api/handlers/v1"
	"gitlab.com/learn-microservice/api-gateway/config"
	"gitlab.com/learn-microservice/api-gateway/pkg/logger"
	"gitlab.com/learn-microservice/api-gateway/services"

	ginSwagger "github.com/swaggo/gin-swagger"
)

// Option ...
type Option struct {
	Conf           config.Config
	Logger         logger.Logger
	ServiceManager services.IServiceManager
}

// New ...

// @title  Go-Bootcamp N7
// @version 1.0

// @host	localhost:7070
func New(option Option) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:         option.Logger,
		ServiceManager: option.ServiceManager,
		Cfg:            option.Conf,
	})

	api := router.Group("/v1")
	// users
	api.POST("/users", handlerV1.CreateUser)
	api.GET("/users/:id", handlerV1.GetUser)
	api.GET("/users", handlerV1.ListUsers)
	api.PUT("/users/:id", handlerV1.UpdateUser)
	api.DELETE("/users/:id", handlerV1.DeleteUser)
	// posts

	api.POST("/post", handlerV1.CreatePost)
	api.GET("/post/:id", handlerV1.GetPost)

	url := ginSwagger.URL("swagger/doc.json")
	api.GET("swagger/*any", ginSwagger.WrapHandler(swaggerfile.Handler, url))


	return router
}
