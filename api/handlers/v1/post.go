package v1

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	p "gitlab.com/learn-microservice/api-gateway/genproto/post"
	"gitlab.com/learn-microservice/api-gateway/pkg/logger"
)

// router-->> /post	[post]
func (h *handlerV1) CreatePost(c *gin.Context) {
	var body p.PostRequest
	err := c.ShouldBindJSON(&body)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "Invalid info",
		},
		)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	data, err := h.serviceManager.PostService().CreatePost(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "Something went wrong",
		})
		return
	}

	c.JSON(http.StatusCreated, data)
}

func (h *handlerV1) GetPost(c *gin.Context) {
	id := c.Param("id")
	postId, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "invalid info",
		})
		return
	}
	fmt.Printf(">>>>>>>>>>>>>> %T, %v", postId, postId)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	info, err := h.serviceManager.PostService().GetPostById(ctx, &p.PostId{
		Id: postId,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "Oops Something went wrong",
		})
		h.log.Error("Error while getting post", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, info)
}
