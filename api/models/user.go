package models

type UserReq struct {
	PostId    int    `json:"post_id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
}

type UserRes struct {
	Id        int    `json:"id"`
	PostId    int    `json:"post_id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
}
