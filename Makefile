run:
	go run cmd/main.go

proto-gen:
	./script/gen-proto.sh

migrate_up:
	migrate -path migrations/ -database postgres://developer:2002@localhost:5432/user_db up



migrate_down:
	migrate -path migrations/ -database postgres://developer:2002@localhost:5432/user_db down


migrate_force:
	migrate -path migrations/ -database postgres://developer:2002@localhost:5432/user_db forse 1


migrate_post:
	migrate create -ext sql -dir migrations -seq create_post_table


migrate_file:
	migrate create -ext sql -dir migrations -seq create_users_table


swag:
	swag init -g ./api/router.go -o api/docs