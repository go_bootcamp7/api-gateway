package services

import (
	"fmt"

	"gitlab.com/learn-microservice/api-gateway/config"
	p "gitlab.com/learn-microservice/api-gateway/genproto/post"
	pb "gitlab.com/learn-microservice/api-gateway/genproto/user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/resolver"
)

type IServiceManager interface {
	UserService() pb.UserServiceClient
	PostService() p.PostServiceClient
}

type serviceManager struct {
	userService pb.UserServiceClient
	postService p.PostServiceClient
}

func (s *serviceManager) UserService() pb.UserServiceClient {
	return s.userService
}

func (s *serviceManager) PostService() p.PostServiceClient {
	return s.postService
}

func NewServiceManager(conf *config.Config) (IServiceManager, error) {
	resolver.SetDefaultScheme("dns")

	connUser, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.UserServiceHost, conf.UserServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	connPost, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.PostServiceHost, conf.PostServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	serviceManager := &serviceManager{
		userService: pb.NewUserServiceClient(connUser),
		postService: p.NewPostServiceClient(connPost),
	}

	return serviceManager, nil
}
